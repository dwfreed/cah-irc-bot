import botconfig
import asyncore, asynchat, ssl, socket, traceback, sys, collections

class IRC(asynchat.async_chat):
	def __init__(self):
		addrinfo = socket.getaddrinfo(botconfig.HOST, botconfig.PORT, socket.AF_UNSPEC, socket.SOCK_STREAM, 0, socket.AI_ADDRCONFIG | socket.AI_V4MAPPED | socket.AI_ALL)
		for family, socktype, protocol, _, address in addrinfo:
			try:
				sock = socket.socket(family, socktype, protocol)
				sock.connect(address)
				break
			except Exception:
				continue
		else:
			sys.exit("Failed to connect to " + botconfig.HOST)

		sock = ssl.wrap_socket(sock, cert_reqs=ssl.CERT_REQUIRED, ssl_version=ssl.PROTOCOL_TLSv1, ca_certs="/etc/ssl/certs/ca-certificates.crt") # TODO: doesn't support RHEL-based
		sock.do_handshake()
		asynchat.async_chat.__init__(self, sock=sock)
		self.ibuffer = []
		self.obuffer = collections.deque()
		self.set_terminator("\r\n")

	def collect_incoming_data(self, data):
		self.ibuffer.append(data)

	def found_terminator(self):
		line = "".join(self.ibuffer)
		self.ibuffer = []
		self.parse(line)

	def parse(self, line):
		temp = line.split(' :', 1)
		prefix, command, params, rest, nick, user, host = (None, None, None, None, None, None, None)
		if len(temp) > 0:
			if temp[0][0] == ':':
				temp2 = temp[0][1:].split(' ')
				prefix = temp2[0]
				temp2 = temp2[1:]
			else:
				temp2 = temp[0].split(' ')
			command = temp2[0]
			params = temp2[1:]
			if len(temp) > 1:
				rest = temp[1]
				params.append(rest)
		if prefix:
			if '!' in prefix:
				nick, user = prefix.split('!')
			if user and '@' in user:
				user, host = user.split('@')
			elif '@' in prefix:
				nick, host = prefix.split('@')
		self.process(line, prefix, nick, user, host, command, params, rest)

	def process(self, line, prefix, nick, user, host, command, params, rest):
		pass
